package fr.uvsq.Hayani.Calculatrice_RPN;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class SaisieRPNTest {
	
	private SaisieRPN calculatrice;

	@Before
	public void setUp() {
		calculatrice = new SaisieRPN();
	}
	
	@Test
	public void IsOperandtest() {
		assertEquals(true,calculatrice.IsOperande("13.3"));
	}
	
	@Test
	public void GetOperationtest() {
		assertEquals(Operation.PLUS,calculatrice.getOperation("+"));
	}
	
	@Test
	public void IsOperationTest() {
		assertEquals(true,calculatrice.IsOperation("+"));
	}
	
	@Test
	public void IsUndotest() {
		assertEquals(true,calculatrice.IsUndo("S"));
	}
	
	@Test
	public void IsCleantest() {
		assertEquals(true,calculatrice.IsClean("clean"));
	}
	
	
	
	
	
	

}
