package fr.uvsq.Hayani.Calculatrice_RPN;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import fr.uvsq.Hayani.Calculatrice_RPN.Exceptions.CalculatorException;
import fr.uvsq.Hayani.Calculatrice_RPN.Exceptions.MaxOperandException;
import fr.uvsq.Hayani.Calculatrice_RPN.Exceptions.MinOperandException;
import fr.uvsq.Hayani.Calculatrice_RPN.Exceptions.PilevideException;



public class MoteurRPNTest {
	private MoteurRPN pile;
	
	@Before
	public void setUp() {
		  pile = new MoteurRPN();
	}
 
	
	@Test
    public void EmpilerTest() throws CalculatorException 
    {
		pile.Empiler(2);
		pile.Empiler(3);
		assertEquals((Double)3.0,(Double)pile.getPile().peek());
       
    }
	
	
	@Test(expected = MinOperandException.class)
    public void EmpilerUneValeurMoinsDeMinValueTest() throws CalculatorException {
		pile.Empiler(-99999999);
        
    }
	
	@Test(expected = MaxOperandException.class)
    public void EmpilerUneValeurPlusDeMaxValueTest() throws CalculatorException {
		pile.Empiler(99999999);
        
    }
	
	@Test
    public void DepilerTest() throws CalculatorException {
		pile.Empiler(1);
		pile.Empiler(2);

		assertEquals((Double)2.0,(Double)pile.depile());
    }
	
	@Test(expected = PilevideException.class)
    public void DepilerPileVideTest() throws CalculatorException {
        pile.depile();
        
    }
	
	@Test
	public void SimpleCalculeTest() throws CalculatorException 
	    {
			pile.Empiler(2);
			pile.Empiler(3);
			pile.Calculer(Operation.PLUS);
			assertEquals((Double)5.0,(Double) pile.getPile().lastElement());
	       
	    }
	 
	 @Test(expected = CalculatorException.class)
	 public void CalculerAvecUnSeulOperandTest() throws CalculatorException 
	    {
			pile.Empiler(2);
			pile.Calculer(Operation.PLUS);
	  }
	 
	 
	 @Test
		public void CalculerAvecPlusieursOperandTest() throws CalculatorException 
		    {
				pile.Empiler(2);
				pile.Empiler(3);
				pile.Empiler(2);
				pile.Empiler(3);
				pile.Calculer(Operation.PLUS);
				pile.Calculer(Operation.PLUS);
				pile.Calculer(Operation.PLUS);
				Double size = (double) pile.getOperandsNumber();
				assertEquals((Double)1.0,(Double)size);
		       
		    }
	
	
	
	
	
	
	
	
	
	
	
	
	

}
