package fr.uvsq.Hayani.Calculatrice_RPN;


import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import fr.uvsq.Hayani.Calculatrice_RPN.Exceptions.CalculatorException;
import fr.uvsq.Hayani.Calculatrice_RPN.Exceptions.DevisionException;


public class OperationTest {
	
	private Double pg;
	private Double pd;
	
	@Before
	public void setUp() {
		pg = new Double(6);
	    pd = new Double(3);
	}
 
	@Test
    public void AdditionTest() throws CalculatorException{
		 assertEquals((Double)(pd+pg),(Double)Operation.PLUS.Eval(pg, pd));
     }
	
	@Test
    public void SoustractionTest() throws CalculatorException{
		assertEquals((Double)(pg-pd),(Double)Operation.MOINS.Eval(pg, pd));
     }
	
	@Test
    public void MultiplicationTest() throws CalculatorException{
		assertEquals((Double)(pg*pd),(Double)Operation.MULT.Eval(pg, pd));
     }
	
	@Test
    public void DivisionTest() throws CalculatorException{
       assertEquals((Double)(pg/pd),(Double)Operation.DIV.Eval(pg, pd));
     }
	
	
	@Test(expected = DevisionException.class)
    public void divideByZero() throws CalculatorException {
		Operation.DIV.Eval(0.0, pd);
       
    }
	
	
   
	
	
	
}
