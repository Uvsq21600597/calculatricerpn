package fr.uvsq.Hayani.Calculatrice_RPN;

import java.util.Scanner;

import fr.uvsq.Hayani.Calculatrice_RPN.Exceptions.CalculatorException;
import fr.uvsq.Hayani.Calculatrice_RPN.Exceptions.DevisionException;
import fr.uvsq.Hayani.Calculatrice_RPN.Exceptions.ExpressionException;

public class SaisieRPN {
	
	private MoteurRPN m;
	
	public SaisieRPN() {
						m=new MoteurRPN();
						}
	
	public boolean IsOperande(String operande)
	{
	try {
		Double value= Double.parseDouble(operande);
					} catch (Exception e) {
											return false;
										}
				return true;
	}
	
	
	public Operation getOperation(String op){
			switch (op) {
			case "+": return Operation.PLUS;
			case "-": return Operation.MOINS;
			case "*": return Operation.MULT;
			case "/": return Operation.DIV;
			default:return null;
			}
	}
		
		
	public boolean IsOperation(String op){
			
		if ( op.equals("-") ||op.equals("+")||op.equals("/")||op.equals("*")  ) return true;
		 return false;
		
	}
	
	public boolean IsUndo(String op){
		if ( op.equals("S")|| op.equals("s") || op.equals("supprimer") ) return true;
		 return false;
	}
	
	public boolean IsClean(String op){
		if ( op.equals("c") || op.equals("clean") ||op.equals("C") ) 	return true;
		return false;
	}
	
	public void lire() throws   CalculatorException, DevisionException, ExpressionException 
	{
		Scanner sc=new Scanner(System.in); 
		int Compteur=0;
		
		do{
			System.out.print("Tapez un nombre ou une operation : ");
			String chaine= sc.nextLine();
			
			if(chaine.equals("exit")) return ;
			
			else if(IsOperande(chaine)){
				m.Empiler(Double.parseDouble(chaine));
			}
			else if (IsClean(chaine)) {
				m=new MoteurRPN();
				Compteur=0;
			}
			else if (IsUndo(chaine)) m.depile();
			else if (IsOperation(chaine)){
				
				
				if ( m.getOperandsNumber()-1>Compteur ) {
					Compteur++;
					m.Calculer(getOperation(chaine));
					Compteur=0;
					
				}
				
				
			}
				
			else throw new ExpressionException();
			System.out.println("**** LA PILE : "+m.getPile()+"  ****");
			System.out.println();
			
		}while(true);
		
		
	}

	public MoteurRPN getMoteur() {
		return m;
	}


	

	
	
}
