package fr.uvsq.Hayani.Calculatrice_RPN;

import java.util.Stack;
import java.util.Vector;

import fr.uvsq.Hayani.Calculatrice_RPN.Exceptions.CalculatorException;
import fr.uvsq.Hayani.Calculatrice_RPN.Exceptions.MaxOperandException;
import fr.uvsq.Hayani.Calculatrice_RPN.Exceptions.MinOperandException;
import fr.uvsq.Hayani.Calculatrice_RPN.Exceptions.PilevideException;


public class MoteurRPN {
	
	private Stack<Double> pile;
	private final static int min_value=-9999999;
	private final static int  max_value=9999999;

	public void Empiler(double operande) throws CalculatorException
	{
		if(min_value>operande) throw new MinOperandException();
		if(max_value<operande) throw new MaxOperandException();
		pile.push(operande);
	
		
		
	}

	public void Calculer(Operation op) throws CalculatorException
	{
		Double pd,pg;
		pd=depile();
		pg=depile();
		pile.push(op.Eval(pd, pg));
			
	}
	

	 public double depile() throws CalculatorException 
		{
		
			if (pile.isEmpty()) throw new PilevideException();
			return pile.pop() ;
			  
		}
	 public int getOperandsNumber()
		{
			return pile.size();
			
		}

	public Stack<Double> getPile() 
		{
			return pile;
			
		}
		
		
		public boolean isEmpty() 
		{
			return pile.empty();
			
		}
	
		public MoteurRPN() 
		{
			pile=new Stack<>();
		}
		
}