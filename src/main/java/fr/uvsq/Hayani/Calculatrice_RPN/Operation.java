package fr.uvsq.Hayani.Calculatrice_RPN;

import fr.uvsq.Hayani.Calculatrice_RPN.Exceptions.CalculatorException;
import fr.uvsq.Hayani.Calculatrice_RPN.Exceptions.DevisionException;

public enum Operation {
	PLUS("+")
	{
		@Override
		public Double Eval(Double PG, Double PD) {return PD+PG;}
	},
	MOINS("-")
	{
		@Override
		public Double Eval(Double PG, Double PD) {return PG-PD;}
	},
	MULT("*")
	{
		@Override
		public Double Eval(Double PG, Double PD) { return PG*PD;}
	},
	DIV("/"){
		
		@Override
		public Double Eval(Double PG, Double PD) throws DevisionException {
		
		
		if (PG == 0) throw new DevisionException();
		return PG/PD;
		}
		
	};
	private String symbole;
	
	private Operation(String symbole) {
		this.symbole = symbole;
	}

	public abstract Double Eval(Double PG, Double PD) throws CalculatorException ;
}
