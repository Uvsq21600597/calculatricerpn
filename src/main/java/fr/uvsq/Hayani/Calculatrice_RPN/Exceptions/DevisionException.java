package fr.uvsq.Hayani.Calculatrice_RPN.Exceptions;

public class DevisionException  extends CalculatorException{

	public DevisionException() {
		super("Impossible de diviser par zero");
	}
}
