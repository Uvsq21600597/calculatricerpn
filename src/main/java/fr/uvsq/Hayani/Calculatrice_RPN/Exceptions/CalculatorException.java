package fr.uvsq.Hayani.Calculatrice_RPN.Exceptions;


public class CalculatorException extends Exception {

	public CalculatorException(String message) {
		super(message);
	}
	
}
