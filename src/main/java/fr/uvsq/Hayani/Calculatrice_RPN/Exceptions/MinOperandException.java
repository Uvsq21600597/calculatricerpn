package fr.uvsq.Hayani.Calculatrice_RPN.Exceptions;



public class MinOperandException extends CalculatorException {
	
	public MinOperandException() {
		super("La valeur est moins que l'intervalle donné");
	}

}
